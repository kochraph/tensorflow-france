import tensorflow as tf
import os
#https://github.com/backstopmedia/tensorflowbook

print "starting..."

# initialize variables/model parameters
W = tf.Variable(tf.zeros([5, 1]), name="weights")
b = tf.Variable(0., name="bias")
b_summary_OP = tf.scalar_summary("b", b)



def read_csv(batch_size, file_name, record_defaults):
    filename_queue = tf.train.string_input_producer([os.path.join(os.getcwd() , file_name)])

    reader = tf.TextLineReader(skip_header_lines=0)
    key, value = reader.read(filename_queue)

    # decode_csv will convert a Tensor from type string (the text line) in
    # a tuple of tensor columns with the specified defaults, which also
    # sets the data type for each column
    decoded = tf.decode_csv(value, record_defaults=record_defaults)

    # batch actually reads the file and loads "batch_size" rows in a single tensor
    return tf.train.shuffle_batch(decoded,
                                  batch_size=batch_size,
                                  capacity=batch_size * 50,
                                  min_after_dequeue=batch_size)



# define the training loop operations
def inference(X):
    # compute inference model over data X and return the result
    return tf.sigmoid(tf.matmul(X, W) + b)

def loss(X, Y):
    # compute loss over training data X and expected outputs Y
    return tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits((tf.matmul(X, W) + b), Y))

def inputs():
    # read/generate input training data X and expected outputs Y

    passenger_id, survived, pclass, name, sex, age, sibsp, parch, ticket, fare, cabin, embarked = read_csv(100, "train.csv", [[0.0], [0.0], [0.0], [""], [""], [0.0], [0.0], [0.0], [""], [0.0], [""], [""]])


#PassengerId,Survived,Pclass,Name,Sex,Age,SibSp,Parch,Ticket,Fare,Cabin,Embarked
#1    ,     0,   3, "Braund, Mr. Owen Harris", male,    22,     1,     0, A/5 21171,  7.25,     ,S
#[0.0], [0.0], [0],                      [""], [""], [0.0], [0.0], [0.0],      [""], [0.0], [""], [""]

    # convert categorical data
    is_first_class = tf.to_float(tf.equal(pclass, [1]))
    is_second_class = tf.to_float(tf.equal(pclass, [2]))
    is_third_class = tf.to_float(tf.equal(pclass, [3]))

    gender = tf.to_float(tf.equal(sex, ["female"]))

    # Finally we pack all the features in a single matrix;
    # We then transpose to have a matrix with one example per row and one feature per column.
    features = tf.transpose(tf.pack([is_first_class, is_second_class, is_third_class, gender, age]))
    survived = tf.reshape(survived, [100, 1])

    return features, survived

def train(total_loss):
    # train / adjust model parameters according to computed total loss
    learning_rate = 0.01
    return tf.train.GradientDescentOptimizer(learning_rate).minimize(total_loss)

def evaluate(sess, X, Y):
    # evaluate the resulting trained model
    predicted = tf.cast(inference(X) > 0.5, tf.float32)

    print " Evaluation : ", sess.run(tf.reduce_mean(tf.cast(tf.equal(predicted, Y), tf.float32)))



# Create a saver.
saver = tf.train.Saver()

# Launch the graph in a session, setup boilerplate
with tf.Session() as sess:

    summary_writer = tf.train.SummaryWriter('./titanic_logs', sess.graph)
    
    tf.initialize_all_variables().run()

    X, Y = inputs()

    total_loss = loss(X, Y)
    total_loss_summary_OP = tf.histogram_summary("total_loss", total_loss)

    train_op = train(total_loss)
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    summaries = tf.merge_all_summaries()

    # actual training loop
    training_steps = 2000
    for step in range(training_steps):
        _,total_loss_session = sess.run([train_op,total_loss])

        # for debugging and learning purposes, see how the loss gets decremented thru training steps
        if step % 10 == 0:
            #total_loss_session = sess.run([total_loss])
            print "loss: ", total_loss_session 

            # Creates summaries 
            
            summary_str = sess.run(summaries)
            summary_writer.add_summary(summary_str,step)
            

        if step % 1000 == 0:
            saver.save(sess, 'my-model', global_step=step)

    evaluate(sess, X, Y)

    saver.save(sess, 'my-model', global_step=training_steps)
    coord.request_stop()
    coord.join(threads)
    sess.close()