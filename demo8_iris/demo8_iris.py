import tensorflow as tf
import os

#https://archive.ics.uci.edu/ml/datasets/Iris

print "starting..."

# initialize variables/model parameters
# this time weights form a matrix, not a vector, with one "feature weights column" per output class.
W = tf.Variable(tf.zeros([4, 3]), name="weights")
# so do the biases, one per output class.
b = tf.Variable(tf.zeros([3], name="bias"))
#b_summary_OP = tf.scalar_summary("b", b)



def read_csv(batch_size, file_name, record_defaults):
    filename_queue = tf.train.string_input_producer([os.path.join(os.getcwd() , file_name)])

    reader = tf.TextLineReader(skip_header_lines=1)
    key, value = reader.read(filename_queue)

    # decode_csv will convert a Tensor from type string (the text line) in
    # a tuple of tensor columns with the specified defaults, which also
    # sets the data type for each column
    decoded = tf.decode_csv(value, record_defaults=record_defaults)


    # batch actually reads the file and loads "batch_size" rows in a single tensor
    return tf.train.shuffle_batch(decoded,
                                  batch_size=batch_size,
                                  capacity=batch_size * 50,
                                  min_after_dequeue=batch_size)


# define the training loop operations
def inference(X):
    # compute inference model over data X and return the result
    return tf.nn.softmax(tf.matmul(X, W) + b)

def loss(X, Y):
    # compute loss over training data X and expected outputs Y
    return tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits((tf.matmul(X, W) + b), Y))

def inputs():

    sepal_length, sepal_width, petal_length, petal_width, label =read_csv(100, "iris.data", [[0.0], [0.0], [0.0], [0.0], [""]])


#[0.0], [0.0], [0.0], [0.0], [""]
#4.4  ,   2.9,   1.4,   0.2, Iris-setosa
    # convert class names to a 0 based class index.
    label_number = tf.to_int32(tf.argmax(tf.to_int32(tf.pack([
        tf.equal(label, ["Iris-setosa"]),
        tf.equal(label, ["Iris-versicolor"]),
        tf.equal(label, ["Iris-virginica"])
    ])), 0))

    # Pack all the features that we care about in a single matrix;
    # We then transpose to have a matrix with one example per row and one feature per column.
    features = tf.transpose(tf.pack([sepal_length, sepal_width, petal_length, petal_width]))

    return features, label_number

def train(total_loss):
    # train / adjust model parameters according to computed total loss
    learning_rate = 0.01
    return tf.train.GradientDescentOptimizer(learning_rate).minimize(total_loss)

def evaluate(sess, X, Y):
    # evaluate the resulting trained model
    predicted = tf.cast(tf.arg_max(inference(X), 1), tf.int32)

    print sess.run(tf.reduce_mean(tf.cast(tf.equal(predicted, Y), tf.float32)))



# Create a saver.
saver = tf.train.Saver()

# Launch the graph in a session, setup boilerplate
with tf.Session() as sess:

    summary_writer = tf.train.SummaryWriter('./iris_logs', sess.graph)
    
    tf.initialize_all_variables().run()

    X, Y = inputs()

    total_loss = loss(X, Y)

    total_loss_summary_OP = tf.histogram_summary("total_loss", total_loss)


    train_op = train(total_loss)
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(sess=sess, coord=coord)

    summaries = tf.merge_all_summaries()

    # actual training loop
    training_steps = 2000
    for step in range(training_steps):
        #print "debug"
        #print X.get_shape()
        #print Y.get_shape()

        #print "------------------X------------------"
        #print sess.run(X)
        #print "------------------Y------------------"
        #print sess.run(Y)

        _,total_loss_session = sess.run([train_op,total_loss])

        #print "stop"

        # for debugging and learning purposes, see how the loss gets decremented thru training steps
        if step % 10 == 0:
            #total_loss_session = sess.run([total_loss])
            print "loss: ", total_loss_session 

            # Creates summaries 
            
            summary_str = sess.run(summaries)
            summary_writer.add_summary(summary_str,step)
            

        if step % 1000 == 0:
            saver.save(sess, 'my-model', global_step=step)

    evaluate(sess, X, Y)

    saver.save(sess, 'my-model', global_step=training_steps)
    coord.request_stop()
    coord.join(threads)
    sess.close()