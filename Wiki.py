import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import pandas as pd


#Initialise Data

i = tf.constant(1.0, name='Entree')

x = tf.constant([1.0, 2.0, 3.0 ])

m = tf.constant([[1.0, 2.0, 3.0, 4.0 ],[11.0, 22.0, 33.0, 44.0 ],[111.0, 222.0, 333.0, 444.0 ]])


#norm = tf.random_normal([3, 4], mean=-1, stddev=4)
norm = tf.Variable(tf.random_normal([3, 4], mean=-1, stddev=4), name="var")

z = tf.Variable(tf.random_normal([2, 2, 3, 3], mean=-1, stddev=4), name="z")




#evaluation
init_op = tf.initialize_all_variables()
with tf.Session() as sess:
	sess.run(init_op)
	#OUTPUT
	print " ----------------------"
	print " ----------------------"
	print " i (constante) : ", sess.run(i)
	print " i (constante)  shape : ", i.get_shape()
	print " i (constante)  ndims : ", i

	print " ----------------------"
	print " ----------------------"
	print " x  : ", sess.run(x)
	print " x   shape : ", x.get_shape()
	print " x   info  : ", x
	print " x[0]  : ", sess.run(x[0])
	print " x[2]  : ", sess.run(x[2])


	print " ----------------------"
	print " ----------------------"
	print " m  : ", sess.run(m)
	print " m   shape : ", m.get_shape()
	print " m   info  : ", m
	print " m[0,0]  : ", sess.run(m[0,0])
	print " m[2,0]  : ", sess.run(m[2,0])
	print " m[1,2]  : ", sess.run(m[1,2])

	print " ----------------------"
	print " ----------------------"
	print " norm  : ", norm.eval()
	print " norm   shape : ", norm.get_shape()
	print " norm   info  : ", norm
	print " norm[0,0]  : ", norm[0,0].eval()
	print " norm[2,0]  : ", norm[2,0].eval()
	print " norm[1,2]  : ", norm[1,2].eval()

	print " ----------------------"
	print " ----------------------"
	print " z  : ", z.eval()
	print " z   shape : ", z.get_shape()
	print " z   info  : ", z
	print " z[0,0,0,0]  : ", z[0,0,0,0].eval()
	print " z[1,1,2,2]  : ", z[1,1,2,2].eval()
	#print " z[2,0]  : ", z[2,0].eval()
	#print " z[1,2]  : ", z[1,2].eval()




