import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import pandas as pd


#Initialise Data


x = tf.constant([[[1.0, 2.0, 0., 4.0 ],[1.0, 0., 3.0, 4.0 ],[1.0, 2.0, 3.0, 0. ]],[[1.0, 2.0, 3.0, 1.0 ],[1.0, 0.5, 3.0, 4.0 ],[0.0, 2.0, 3.0, 0.1 ]]])

result = tf.reduce_sum(x)
#evaluation
init_op = tf.initialize_all_variables()
with tf.Session() as sess:
	sess.run(init_op)
	#OUTPUT


	print " ----------------------"
	print " ----------------------"
	print " x  : "
	print x.eval()
	print " x   shape : ", x.get_shape()
	print " x   info  : ", x
	print " x[0,0,1]  : ", x[0,0,1].eval()
	print " x[1,0,2]  : ", x[1,0,2].eval()
	print " x[1,1,0]  : ", x[1,1,0].eval()


	print " ----------------------"
	print " ----------------------"

	print " tf.reduce_sum(x)  : "
	print result.eval()

	print " tf.reduce_sum(x,0).eval()  : "
	print tf.reduce_sum(x,0).eval()

	print " tf.reduce_sum(x,1).eval()  : "
	print tf.reduce_sum(x,1).eval()

	print " tf.reduce_sum(x,2).eval()  : "
	print tf.reduce_sum(x,2).eval()






