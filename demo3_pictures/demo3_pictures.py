import matplotlib.image as mp_image 
filename = "lookiv.fr.png" 
input_image = mp_image.imread(filename)

#print input_image

print 'input dim = {}'.format(input_image.ndim) 
print 'input shape = {}'.format(input_image.shape)

import matplotlib.pyplot as plt 
plt.imshow(input_image) 
#plt.show()

import tensorflow as tf 
my_image = tf.placeholder("uint8",[None,None,4])
x = tf.Variable(input_image,name='x')

model = tf.initialize_all_variables()

slice = tf.slice(my_image,[10,0,0],[16,-1,-1])

with tf.Session() as session:
	result = session.run(slice,feed_dict={my_image: input_image})

	x = tf.transpose(x, perm=[1,0,2])
	session.run(model)
	result=session.run(x)
	
	print(result.shape) 
 
plt.imshow(result) 
plt.show()