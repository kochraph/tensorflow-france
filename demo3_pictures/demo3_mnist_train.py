import numpy as np
import tensorflow as tf
import input_data
import matplotlib.pyplot as plt

#Build the Training Set
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

train_pixels,train_list_values = mnist.train.next_batch(100) 
test_pixels,test_list_of_values  = mnist.test.next_batch(10) 

train_pixel_tensor = tf.placeholder("float", [None, 784])
test_pixel_tensor = tf.placeholder("float", [784])

#DEBUG TRAIN
print train_pixels[50]
print train_list_values[50]
image = train_pixels[50,:]
image = np.reshape(image,[28,28])
plt.imshow(image)
#plt.show()

#DEBUG
print test_pixels[3]
print test_list_of_values[3]
image = test_pixels[3,:]
image = np.reshape(image,[28,28])
plt.imshow(image)
#plt.show()

#Cost Function and distance optimization
distance = tf.reduce_sum(tf.abs(tf.add(train_pixel_tensor, tf.neg(test_pixel_tensor))),reduction_indices=1)
pred = tf.arg_min(distance, 0)

# Testing and algorithm evaluation
accuracy = 0.
init = tf.initialize_all_variables()

with tf.Session() as sess:
	sess.run(init)
	for i in range(len(test_list_of_values)):
		print sess.run(distance,feed_dict={train_pixel_tensor:train_pixels,test_pixel_tensor:test_pixels[i,:]})

		#distance = test pixel - train pixel (batchsize)
		
		nn_index = sess.run(pred,feed_dict={train_pixel_tensor:train_pixels,test_pixel_tensor:test_pixels[i,:]})
		print "nn_index : ", nn_index
		# nn_index =  index dans le training set se rapprochant le plus du test.
		
		print "Test N: ", i,"Predicted Class: ", np.argmax(train_list_values[nn_index]),"True Class: ", np.argmax(test_list_of_values[i])
		if np.argmax(train_list_values[nn_index])== np.argmax(test_list_of_values[i]):
			accuracy += 1./len(test_pixels)
print "Result = ", accuracy
