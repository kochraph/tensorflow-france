#tensorboard --logdir=log_simple_graph/


import numpy as np
import matplotlib.pyplot as plt
import seaborn
import tensorflow as tf


# Define input data
X_data = np.arange(100, step=.1)
#X_data = [x,x,x,x,x,x,x,x,x,x,x,......x,x,x,x]
print X_data.shape #1000
print X_data.ndim  #1
print X_data.dtype #dfloat
print X_data

y_data = X_data + 20 * np.sin(X_data/10)
#print y_data.shape
#print y_data.ndim
#print y_data.dtype
#print y_data

# Plot input data
plt.scatter(X_data, y_data)
#plt.show()

# Define data size and batch size
n_samples = 1000
batch_size = 100

# Tensorflow is finicky about shapes, so resize
X_data = np.reshape(X_data, (n_samples,1))
#X_data = 
#[[  0. ]
# [  0.1]
# [  0.2]
# [  0.3]
# [  0.4]
# [  0.5]
# [  .]
# [  .]
# [  x]
# [  x]
#print X_data.shape  #1000,1
#print X_data.ndim   #2
#print X_data.dtype  #dfloat
#print X_data

y_data = np.reshape(y_data, (n_samples,1))
#print y_data.shape
#print y_data.ndim
#print y_data.dtype
#print y_data

# Define placeholders for input
X = tf.placeholder(tf.float32, shape=(batch_size, 1))
y = tf.placeholder(tf.float32, shape=(batch_size, 1))

# Define variables to be learned
with tf.variable_scope("linear-regression"):
	W = tf.get_variable("weights",(1, 1),initializer=tf.random_normal_initializer())
	b = tf.get_variable("bias", (1,),initializer=tf.constant_initializer(0.0))
	y_pred = tf.matmul(X, W) + b

	loss = tf.reduce_sum((y - y_pred)**2/n_samples,name='loss')
	
	loss_summ = tf.scalar_summary("loss", loss)

	print "loss shape"
	print loss.get_shape
	
	#tensorboard
	for value in [W,b]:
		print " VALUES:"
		print value.op.name
		print "value :" , value
		#tf.scalar_summary(value.op.name, value)
		#w1 = tf.Variable(tf.zeros([1]),name="a",trainable=True)
		tf.histogram_summary(value.op.name,value)
	summaries = tf.merge_all_summaries()




# Sample code to run full gradient descent:
# Define optimizer operation
opt_operation = tf.train.AdamOptimizer().minimize(loss)

with tf.Session() as sess:
	# Initialize Variables in graph
	sess.run(tf.initialize_all_variables())

	print "In Session ============="
	print sess.run(W)
	print W.get_shape()
	print sess.run(b) 
	print b.get_shape() 

	#tensorboard
	summary_writer = tf.train.SummaryWriter('log_simple_graph', sess.graph)
	#print "GET SHAPE LOSS"
	#print sess.run(loss)
	#print loss.get_shape()

	# Gradient descent loop for 500 steps
	for i in range(500):
		# Select random minibatch
		indices = np.random.choice(n_samples, batch_size)
		print " === indices =="
		print indices
		print indices.shape
		print indices.ndim
		print indices.dtype
		X_batch, y_batch = X_data[indices], y_data[indices]
		print " === X_batch =="
		print X_batch
		print X_batch.shape
		print X_batch.ndim
		print " === y_batch =="
		print y_batch
		print y_batch.shape
		print y_batch.ndim

		# Do gradient descent step
		#something , loss_val = sess.run([opt_operation, loss,summaries], feed_dict={X: X_batch, y: y_batch})

		
		_,loss_val,summary_str = sess.run([opt_operation, loss,summaries], feed_dict={X: X_batch, y: y_batch})	
		
		
		#tf.scalar_summary('loss_val', loss_val)

		#tf_loss = tf.convert_to_tensor(loss_val, dtype=tf.float32)
		#tf.scalar_summary(tf_loss.name, tf_loss.eval())
		#summaries = tf.merge_all_summaries()
		print "loss_val"
		print loss_val

		W_f = tf.to_float(W, name='ToFloat').eval()
		b_f = tf.to_float(b, name='bias').eval()
		print W_f[0][0]
		print b_f[0]
		print y_pred
		#tensorboard
		#print loss_val
		summary_writer.add_summary(summary_str,i)


	
	print "====final value======"
	print W_f[0][0]
	print b_f[0]
	print loss_val
	print type(loss_val)

	print "In Session ============="
	print sess.run(W)
	print W.get_shape()
	print sess.run(b) 
	print b.get_shape()

	#Resultat
	# Define input data
	X_data2 = np.arange(100, step=.1)
	#print X_data2

	y_data2 = X_data2*W_f[0][0]+b_f[0]
	#print y_data2

	# Plot input data
	plt.scatter(X_data2, y_data2)
	plt.show()



