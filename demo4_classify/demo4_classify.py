import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import pandas as pd


def display_partition(x_values,y_values,assignment_values):
	labels = []
	colors = ["red","blue","green","yellow"]
	for i in xrange(len(assignment_values)):
		labels.append(colors[(assignment_values[i])])
	color = labels
	df = pd.DataFrame(dict(x =x_values,y = y_values ,color = labels ))
	fig, ax = plt.subplots()
	ax.scatter(df['x'], df['y'], c=df['color'])
	plt.show()

#We must define the parameters of our problem. The total number of points that we want to cluster is 1000 points:
num_vectors = 1000

#The number of partitions you want to achieve by all initial:
num_clusters = 4

#We set the number of computational steps of the k-means algorithm:
num_steps = 800

#We initialize the initial input data structures:
x_values = []
y_values = []
vector_values = []

#The training set creates a random set of points, 
#which is why we use the random.normal NumPy function, allowing us to build the x_values and y_values vectors:

for i in xrange(num_vectors):
	if np.random.random() > 0.5:
		x_values.append(np.random.normal(0.4, 0.7))
		y_values.append(np.random.normal(0.2, 0.8))
	else:
  		x_values.append(np.random.normal(0.6, 0.4))
  		y_values.append(np.random.normal(0.8, 0.5))

#display values
#print x_values
#print y_values

#We use the Python zip function to obtain the complete list of vector_values:
vector_values = zip(x_values,y_values)
#print vector_values
#Then vector_values is converted into a constant, usable by TensorFlow:
vectors = tf.constant(vector_values)
print " === vector === "
print vectors

#We can see our training set for the clustering algorithm with the following commands:
plt.plot(x_values,y_values, '.', label='Input Data')
plt.legend()
#plt.show()

#After randomly building the training set, we have to generate (k = 4) centroid, then determine an index using tf.random_shuffle:
n_samples = tf.shape(vector_values)[0]
#https://www.tensorflow.org/versions/r0.10/api_docs/python/constant_op.html#random_shuffle
random_indices = tf.random_shuffle(tf.range(0, n_samples))
random_indices_var = tf.Variable(random_indices,validate_shape=False)

#By adopting this procedure, we are able to determine four random indices:
begin = [0,]
print begin

size = [num_clusters,]
print size

size[0] = num_clusters
print size

#They have their own indexes of our initial centroids
#https://www.tensorflow.org/versions/r0.10/api_docs/python/array_ops.html#slice
centroid_indices = tf.slice(random_indices, begin, size)
centroids_ind_var = tf.Variable(centroid_indices)
#ici on prend les 4 premiers indices de random indices
#example:
#[932 523 185 814]

#
#https://www.tensorflow.org/versions/r0.10/api_docs/python/array_ops.html#gather
centroids = tf.Variable(tf.gather(vector_values, centroid_indices))
#ici pour chaque indice de centroid_indices, on prend les valeurs dans vector_values a l'indice specifie:
#example:
# centroids :  
#[[ 0.7896089   0.22163406]
# [ 0.81655711  0.83344203]
# [ 0.87664068  1.22728384]
# [ 0.73344457  0.31321409]]

#In order to manage the tensors defined previously, vectors and centroids, 
#we use the TensorFlow function expand_dims, which automatically expands the size of the two arguments
expanded_vectors = tf.expand_dims(vectors, 0)
expanded_centroids = tf.expand_dims(centroids, 1)



#This function allows you to standardize the shape of the two tensors, 
#in order to evaluate the difference by the tf.sub method:
vectors_subtration = tf.sub(expanded_vectors,expanded_centroids)


#Finally, we build the euclidean_distances cost function, 
#using the tf.reduce_sum function, which computes the sum of elements across the dimensions of a tensor, 
#while the tf.square function computes the square of the vectors_subtration element-wise tensor:

euclidean_distances = tf.reduce_sum(tf.square(vectors_subtration), 2)
assignments = tf.to_int32(tf.argmin(euclidean_distances, 0))

#Here assignments is the value of the index with the smallest distance across the tensor euclidean_distances. 
#Let us now turn to the optimization phase, the purpose of which is to improve the choice of centroids, 
#on which the construction of the clusters depends. We partition the vectors (which is our training set) into num_clusters tensors, 
#using indices from assignments.

#The following code takes the nearest indices for each sample, and grabs those out as separate groups using tf.dynamic_partition:

partitions = tf.dynamic_partition(vectors, assignments, num_clusters)

#Finally, we update the centroids, using tf.reduce_mean on a single group to find the average of that group, forming its new centroid:

update_centroids = tf.concat(0, [tf.expand_dims(tf.reduce_mean(partition, 0), 0) for partition in partitions])

#To form the update_centroids tensor, we use tf.concat to concatenate the single one.


#evaluation
init_op = tf.initialize_all_variables()
sess = tf.Session()
sess.run(init_op)


#DEBUG INFO
print " n_samples : ", sess.run(n_samples)
#print " random_indices :" ,  sess.run(random_indices)

print " random_indices_var :" ,  sess.run(random_indices_var)
print " random_indices_var get_shape():" ,  random_indices.get_shape()

print " centroid indixes :" ,sess.run(centroid_indices)
print " centroid indixes shape:" ,centroid_indices.get_shape()

print " centroids : ", sess.run(centroids)
print " centroids shape: ", centroids.get_shape()
print " centroid centroids_ind_var : " , sess.run(centroids_ind_var)

print " expanded_centroids :", sess.run(expanded_centroids)
print " expanded_centroids get shape:", expanded_centroids.get_shape()


print " expanded_vectors : ", sess.run(expanded_vectors)
print " expanded_vectors get shape: ", expanded_vectors.get_shape()


print " vectors_subtration :", sess.run(vectors_subtration)
print " vectors_subtration shape:", vectors_subtration.get_shape()
print "tf.square(vectors_subtration) : ", sess.run(tf.square(vectors_subtration))


print "euclidean_distances:",sess.run(euclidean_distances)
print "euclidean_distances shape :",euclidean_distances.get_shape()
print "assignments:",sess.run(assignments)
print "assignments shape :",assignments.get_shape()

print " partitions : " , sess.run(partitions)

print " update centroids : ", sess.run(update_centroids)

for step in xrange(num_steps):
	_, centroid_values, assignment_values =sess.run([update_centroids,centroids,assignments])

#To display the result, we implement the following function:


print "final============"
print centroid_values


display_partition(x_values,y_values,assignment_values)










