import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import pandas as pd


#Initialise Data

norm = tf.Variable(tf.random_normal([3,2,3], mean=-1, stddev=4), name="var")

begin = [1,0,1]
size = [1,2,2]
after_slice = tf.slice(norm, begin, size)




#evaluation
init_op = tf.initialize_all_variables()
with tf.Session() as sess:
	sess.run(init_op)
	#OUTPUT


	print " ----------------------"
	print " ----------------------"
	print " norm  : ", norm.eval()
	print " norm   shape : ", norm.get_shape()
	print " norm   info  : ", norm
	print " norm[0,0,0]  : ", norm[0,0,0].eval()
	print " norm[2,0,0]  : ", norm[2,0,0].eval()
	print " norm[1,1,2]  : ", norm[1,1,2].eval()


	print " ----------------------"
	print " ----------------------"
	print " begin : " ,begin
	print " size : ", size
	print " after_slice  : ", after_slice.eval()
	print " after_slice   shape : ", after_slice.get_shape()
	print " after_slice   info  : ", after_slice






