import tensorflow as tf



#Utilisation de TensorBoard
x = tf.constant(1.0, name='Entree')
w = tf.Variable(0.75, name='poids')
y = tf.mul(w, x, name='sortie')

y_objectif = tf.constant(0.0, name='y_objectif')

loss = tf.pow(y - y_objectif, 2, name='loss')

train_step = tf.train.GradientDescentOptimizer(0.025).minimize(loss)

for value in [x, w, y, y_objectif, loss]:
    tf.scalar_summary(value.op.name, value)

summaries = tf.merge_all_summaries()

sess = tf.Session()
sess.run(tf.initialize_all_variables())

summary_writer = tf.train.SummaryWriter('log_simple_graph', sess.graph)

for i in range(100):
	summary_writer.add_summary(sess.run(summaries),i)
	sess.run(train_step)
