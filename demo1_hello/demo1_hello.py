import tensorflow as tf

graph = tf.get_default_graph()
print graph.get_operations()

input_value = tf.constant(1.0)
operations = graph.get_operations()
print operations
print operations[0].node_def
print input_value

sess = tf.Session()
print sess.run(input_value)

weight = tf.Variable(0.75)
for op in graph.get_operations(): print(op.name)

output_value = weight * input_value
op = graph.get_operations()[-1]
print op.name
for op_input in op.inputs: print(op_input)

#to get all operations name:
for op_input2 in graph.get_operations(): print(op_input2.name)

init = tf.initialize_all_variables()
sess.run(init)
print sess.run(output_value)

#Utilisation de TensorBoard
x = tf.constant(1.0, name='Entree')
w = tf.Variable(0.75, name='poids')
y = tf.mul(w, x, name='sortie')

y_objectif = tf.constant(0.0)
loss = (y - y_objectif)**2
optim = tf.train.GradientDescentOptimizer(learning_rate=0.025)

grads_and_vars = optim.compute_gradients(loss)
sess.run(tf.initialize_all_variables())
print sess.run(grads_and_vars[1][0])


summary_writer = tf.train.SummaryWriter('log_simple_graph', sess.graph)
